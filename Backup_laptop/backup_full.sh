#!/bin/bash

# Check to see if USB backup drive is mounted
lsusb | grep -i "0bc2:ab30" || exit 1

#Taget dest dir for backups
backup_dir="/media/andrew/Seagate Backup Plus Drive/Linux/Backups/full"

#New backup file
backup_file=home.`date +%Y-%m-%d`.tgz

#Incremental backup file name
backup_inc_file=`basename $backup_file .tgz`.snar

if [[ -d "$backup_dir" ]]
then
    cd "$backup_dir"
    tar --listed-incremental $backup_inc_file -zcvf ./$backup_file /home
    cp $backup_inc_file $backup_inc_file.bak
else
   echo "Could not find dir $backup_dir"
fi  
