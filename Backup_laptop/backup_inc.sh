#!/bin/bash

lsusb | grep -i "0bc2:ab30" || exit 1

device_dir="/media/andrew/Seagate Backup Plus Drive/Linux/Backups"
backup_dir="$device_dir/full"
backup_full_file=`ls -1rt "$backup_dir"/*.snar | tail -1`
backup_inc_dir="incremental"
backup_inc_file=`basename "$backup_full_file" .tgz`.inc-`date +%Y-%m-%d`.tgz
if [[ -d "$backup_dir" && -d "$device_dir"/$backup_inc_dir ]]
then
    echo $backup_dir
    cd "$backup_dir"
    snar_file=`basename "$backup_full_file"`
    echo "Snar file: $snar_file"
    cp $snar_file.bak $snar_file
    tar --listed-incremental $snar_file -zcvf ../$backup_inc_dir/$backup_inc_file /home
else
   echo "Could not find dir $backup_dir"
fi  
