# Backup_laptop

These scripts are intended to help me stay on top of backups for my laptop. 
I have a portable drive I'm using for backups of the home area. 

This is not intended to create a full backup of the drive - which I do want to 
do. One of the great things of my old Macbook Pro was mounting an external 
drive, mirroring the internal drive to it, then making it bootable. You had a 
nice, testable bacup - and in the event of drive failure, you just swapped the 
backup into the laptop.

Again, these scripts are meant to be a stop-gap until I work that out for my new windows/linux dual-boot machine.

In the mean time, these scripts are called by cron if and only if the backup 
drive is mounted at a certain time of day (2am). Incremental backups are called
six days a week (all lvl1 backups, or diffs to the full backup), and a full 
backup done on the 7th. Incremental backups are always releative to the last 
full backup.


## Sample Cron entry
```
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
0 2 * * 6 /root/bin/backup_full.sh > /dev/null 
0 2 * * 0,1,2,3,4,5 /root/bin/backup_inc.sh > /dev/null
# 

```

## To Do:
 * Make a single script? Call w/ options, or know what to do by time-since-full?
 * clean up inc script
